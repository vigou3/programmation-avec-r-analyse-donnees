%%% Copyright (C) 2019-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «IFT-4902 Programmation avec R pour l'analyse de données»
%%% https://gitlab.com/vigou3/programmation-avec-r-analyse-donnees
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Tri et recherche}

\begin{frame}
  \frametitle{Pourquoi étudier les algorithmes de tri et de
    recherche?}

  \begin{quote}
    Every program depends on algorithms and data structures, but few
    programs depend on the invention of brand new ones. \\
    \hfill\small\upshape
    -- Brian Kernighan et Rob Pike, \emph{The Practice of Programming}
  \end{quote}
  \pause

  \begin{itemize}
  \item Algorithmes parmi les plus utilisés
  \item Plusieurs variantes
  \item Hautement optimisés
  \end{itemize}
\end{frame}

\begin{frame}[standout]
  Démo
\end{frame}

\begin{frame}[plain]
  \begin{textblock*}{\paperwidth}(0mm,0mm)
    \includegraphics[width=160mm,keepaspectratio]{images/main-cartes}
  \end{textblock*}

  \begin{textblock*}{\paperwidth}(0mm,40mm)
    \pgfsetfillopacity{0.6}
    \textcolor{white}{\rule{\linewidth}{10mm}}
    \pgfsetfillopacity{1}
  \end{textblock*}

  \begin{textblock*}{\paperwidth}(0mm,40mm)
    \video{https://youtu.be/14c5N8CfnUo}{Illustration des algorithmes de tri}
  \end{textblock*}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Analyse d'une mise en œuvre}

  \begin{minipage}{0.48\linewidth}
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily\NoAutoSpacing]
`{\only<2>{\color{alert}}Selectionsort(Data: values[])}'
  `{\only<4>{\color{alert}}For i = 0 To <length of values> - 1}'
    `{\only<5>{\color{alert}}<Find the smallest item}'
     `{\only<5>{\color{alert}}with index j >= i.>}'
    `{\only<6>{\color{alert}}<Swap values[i] and values[j].>}'
  `{\only<4>{\color{alert}}{}Next i}'
`{\only<2>{\color{alert}}End Selectionsort}'
\end{lstlisting}
  \end{minipage}
  \hfill
  \begin{minipage}{0.48\linewidth}
\begin{Schunk}
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily\NoAutoSpacing]
`{\only<2>{\color{alert}}selectionsort <- function(x)}'
`{\only<2>{\color{alert}}\{}'
  `{\only<3>{\color{alert}}xlen <- length(x)}'
  `{\only<4>{\color{alert}}for (i in seq\_len(xlen))}'
  `{\only<4>{\color{alert}}\{}'
    `{\only<5>{\color{alert}}i.min <- i}'
    `{\only<5>{\color{alert}}for (j in i:xlen)}'
    `{\only<5>{\color{alert}}\{}'
      `{\only<5>{\color{alert}}if (x[j] < x[i.min])}'
        `{\only<5>{\color{alert}}i.min <- j}'
    `{\only<5>{\color{alert}}\}}'
    `{\only<6>{\color{alert}}x[c(i, i.min)] <- x[c(i.min, i)]}'
  `{\only<4>{\color{alert}}\}}'
  `{\only<2>{\color{alert}}x}'
`{\only<2>{\color{alert}}\}}'
\end{lstlisting}
\end{Schunk}
  \end{minipage}

  \onslide<6>
  \begin{center}
    \textbf{Quiz express}\quad
    Identifiez les accolades optionnelles dans le code.
  \end{center}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Mise en œuvre alternative}

  \begin{minipage}{0.48\linewidth}
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily\NoAutoSpacing]
Selectionsort(Data: values[])
  For i = 0 To <length of values> - 1
    `{\color{alert}<Find the smallest item}'
     `{\color{alert}with index j >= i.>}'
    <Swap values[i] and values[j].>
  Next i
End Selectionsort
\end{lstlisting}
  \end{minipage}
  \hfill
  \begin{minipage}{0.48\linewidth}
\begin{Schunk}
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily\NoAutoSpacing]
selectionsort <- function(x)
{
  xlen <- length(x)
  for (i in seq_len(xlen))
  {
    `{\color{alert}i.min <- (i - 1) +}'
        `{\color{alert} which.min(x[i:xlen])}'
    x[c(i, i.min)] <- x[c(i.min, i)]
  }
  x
}
\end{lstlisting}
\end{Schunk}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Un autre exemple de fonction récursive}

  Soit la fonction gamma incomplète
  \begin{equation*}
    \Gamma(a, x) = \int_x^\infty t^{a-1} e^{-t}\, dt,
    \quad x > 0, \quad \alert{a \in \mathbb{R}}.
  \end{equation*}

  En intégrant par parties, on obtient
  \begin{equation*}
    \Gamma(a, x) = -\frac{x^a e^{-x}}{a} + \frac{1}{a} \Gamma(a + 1, x).
  \end{equation*}

  Quand $a < 0$, cette relation peut être utilisée $k$ fois jusqu'à ce
  que $a + k > 0$.

  Par contre, si $a = 0, -1, -2, \dots$, ce calcul requiert la valeur
  de
  \begin{equation*}
    G(0, x) = \int_x^\infty \frac{e^{-t}}{t}\, dt = E_1(x),
  \end{equation*}
  la fonction \alert{intégrale exponentielle}.
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "programmation-avec-r-analyse-donnees"
%%% coding: utf-8
%%% End:
