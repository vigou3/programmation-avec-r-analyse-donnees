%%% Copyright (C) 2017-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «IFT-4902 Programmation avec R pour l'analyse de données»
%%% https://gitlab.com/vigou3/programmation-avec-r-analyse-donnees
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage[noae]{Sweave}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{fontawesome5}              % icônes \fa*
  \usepackage{awesomebox}                % \tipbox et autres
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{relsize}                   % \smaller et al.
  \usepackage{array}                     % lignes centrées vert.
  \usepackage{pict2e}                    % graphiques environnements
  \usepackage{pst-solides3d}             % données et application
  \let\clipbox\relax                     % éviter conflit avec adjustbox
  \usepackage{listings}                  % code source
  \usepackage{framed}                    % env. leftbar
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{pgfgantt}                  % diagramme de Gantt
  \usepackage{metalogo}                  % logo \XeLaTeX
  \usepackage{icomma}                    % virgule intelligente

  %% ============================
  %%  Information de publication
  %% ============================
  \title{IFT-4902 Programmation avec R pour l'analyse de données}
  \author{Vincent Goulet}
  \renewcommand{\year}{2025}
  \renewcommand{\month}{02}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/programmation-avec-r-analyse-donnees/}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Thème Beamer
  \usetheme{metropolis}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \setmathfont{Fira Math}
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Corriger l'espacement entre les éléments de la table des matières
  %% (qui est longue, ici); https://tex.stackexchange.com/a/170438
  \makeatletter
  \patchcmd{\beamer@sectionintoc}{\vfill}{\vskip\itemsep}{}{}
  \makeatother

  %% Couleurs
  \definecolor{comments}{rgb}{0.7,0,0} % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}   % liens internes
  \definecolor{url}{rgb}{0.6,0,0}      % liens externes
  \colorlet{indexedface}{Orchid2}      % cellule indexée (face)
  \colorlet{indexedtop}{Orchid3}       % cellule indexée (dessus)
  \colorlet{indexedside}{Orchid4}      % cellule indexée (côté)
  \colorlet{mindexedface}{MediumOrchid2} % cellule indexée médium (face)
  \colorlet{mindexedtop}{MediumOrchid3}  % cellule indexée médium (dessus)
  \colorlet{mindexedside}{MediumOrchid4} % cellule indexée médium (côté)
  \colorlet{dindexedface}{DarkOrchid2} % cellule indexée foncé (face)
  \colorlet{dindexedtop}{DarkOrchid3}  % cellule indexée foncé (dessus)
  \colorlet{dindexedside}{DarkOrchid4} % cellule indexée foncé (côté)
  \colorlet{analyse}{MediumPurple2}    % diagramme de Gantt
  \colorlet{programmation}{Orchid2}    % diagramme de Gantt
  \colorlet{algorithme}{PaleGreen2}    % diagramme de Gantt
  \colorlet{cafesavoir}{LightBlue2}    % diagramme de Gantt
  \colorlet{livraison}{Orange2}        % diagramme de Gantt
  \colorlet{codebg}{LightYellow1}      % fond code R
  \definecolor{rouge}{rgb}{0.9,0,0.1}  % bandeau rouge UL
  \definecolor{or}{rgb}{1,0.8,0}       % bandeau or UL
  \colorlet{alert}{mLightBrown}        % alias de couleur Metropolis
  \colorlet{dark}{mDarkTeal}           % alias de couleur Metropolis
  \colorlet{code}{mLightGreen}         % alias de couleur Metropolis
  \colorlet{metropolisbg}{black!2}     % couleur de fond Metropolis
  \colorlet{shadecolor}{codebg}

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {IFT-4902 Programmation avec R pour l'analyse de données},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsection}}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% Sections de code source
  \lstloadlanguages{R}
  \lstset{language=R,
    extendedchars=true,
    basicstyle=\small\ttfamily\NoAutoSpacing,
    commentstyle=\color{comments}\slshape,
    keywordstyle=\mdseries,
    escapeinside=`',
    aboveskip=0pt,
    belowskip=0pt,
    showstringspaces=false}

  %%% =========================
  %%%  Nouveaux environnements
  %%% =========================

  %% Environnements de Sweave.
  %%
  %% Les environnements Sinput et Soutput utilisent Verbatim (de
  %% fancyvrb). On les réinitialise pour enlever la configuration par
  %% défaut de Sweave, puis on réduit l'écart entre les blocs Sinput
  %% et Soutput.
  \DefineVerbatimEnvironment{Sinput}{Verbatim}{}
  \DefineVerbatimEnvironment{Soutput}{Verbatim}{}
  \fvset{fontsize=\small,listparameters={\setlength{\topsep}{0pt}}}

  %% L'environnement Schunk est complètement redéfini en un hybride
  %% des environnements snugshade* et leftbar de framed.
  \makeatletter
  \renewenvironment{Schunk}{%
    \def\FrameCommand##1{\hskip\@totalleftmargin
      \vrule width 3pt\colorbox{codebg}{\hspace{5pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% Environnements de type théorème
  \theoremstyle{definition}
  \newtheorem{algorithm}{Algorithme}

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}
  \newcommand{\pkg}[1]{\textbf{#1}}
  \newcommand{\link}[2]{\href{#1}{#2~{\smaller\faExternalLink*}}}
  \newcommand{\meta}[1]{\ensuremath\langle{\normalfont\itshape #1\/}\ensuremath\rangle}

  %% Renvois vers les scripts R
  \newcommand{\gotoR}[1]{%
    \begin{center}
      \colorbox{mDarkTeal}{\color{white}
      \makebox[40mm][c]{%
        \makebox[5mm]{\raisebox{-1pt}{\large\faChevronCircleDown}}\;%
        {\ttfamily #1}}}
    \end{center}}

  %% Renvois vers vidéos YouTube
  \newcommand{\video}[2]{%
    \begin{center}
      \href{#1}{%
        \makebox[5mm]{\raisebox{-2pt}{\Large\faYoutube}}\;{#2}}
    \end{center}}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

%  \includeonly{}

\begin{document}

%% frontmatter
\include{couverture-avant}
\include{notices}

\begin{frame}
  \frametitle{Sommaire}
  \begin{columns}[t]
    \begin{column}{.5\textwidth}
      \tableofcontents[sections={1-7}]
    \end{column}
    \begin{column}{.5\textwidth}
      \tableofcontents[sections={8-13}]
    \end{column}
  \end{columns}
\end{frame}

%% mainmatter
\include{cours}
\include{comment}
\include{travail-longitudinal}
\include{algorithmes}
\include{bases}
\include{donnees}
\include{pratiques}
\include{boucles}
\include{tri}
\include{debogage}
\include{import-export}
\include{texte}
\include{environnement}
\include{epilogue}

%% backmatter
\include{colophon}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: t
%%% End:
