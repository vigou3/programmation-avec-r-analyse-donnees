%%% Copyright (C) 2019-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «IFT-4902 Programmation avec R pour l'analyse de données»
%%% https://gitlab.com/vigou3/programmation-avec-r-analyse-donnees
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% http://creativecommons.org/licenses/by-sa/4.0/

\section{Algorithmes et algorithmique}

\begin{frame}
  \frametitle{Algorithme}

  Procédure de calcul permettant de résoudre un problème bien spécifié.

  \begin{center}
    entrants
    \quad\faArrowRight\quad
    {\LARGE\faCogs}
    \quad\faArrowRight\quad
    sortants
  \end{center}
  \begin{itemize}
  \item a.k.a.\ \alert{recette} de calcul
  \item Explique \alert{quoi} faire, mais pas \alert{comment} le faire
  \item Indépendant d'un langage de programmation
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Pourquoi étudier l'algorithmique?}

  \begin{itemize}
  \item Apprendre le «langage» des algorithmes
  \item Structurer ses pensées
  \item Segmenter son travail
  \item Optimiser son code
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Itération et récursion}

  Deux grandes façons de répéter des calculs en programmation.
  \begin{description}
  \item[Itérativité] bloc d'instructions répété avec un contrôle par
    \begin{itemize}
    \item \alert{dénombrement} (boucle «Pour»), ou
    \item \alert{condition} (boucle «Tant que»)
    \end{itemize}
  \item[Récursion] procédure s'invoque elle-même
    \begin{itemize}
    \item prévoir une \alert{condition d'arrêt}
    \item beaucoup utilisée en Lisp
    \item peu efficace en R (malheureusement?)
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Exemple: calcul de la racine carrée}

  Attardons-nous au problème du calcul de $\sqrt{x}$.

  \begin{itemize}
  \item Nous devons calculer le nombre $y$ tel que $y \geq 0$ et
    $y^2 = x$
  \item Méthode des approximations successives de Newton: si $y$ est
    une approximation de $\sqrt{x}$, alors
    \begin{equation*}
      \frac{y + x/y}{2}
    \end{equation*}
    est une \alert{meilleure} approximation
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Algorithme --- version en langage naturel}

  Calculer la racine carrée d'un nombre positif $x$, c'est-à-dire la
  valeur $y$ tel que $y \geq 0$ et $y^2 = x$.
  \begin{enumerate}
  \item Poser $y$ égal à une valeur de départ quelconque
  \item Si $y^2 \approx x$, retourner la valeur $y$
  \item Poser $y \leftarrow (y + x/y)/2$ et retourner à l'étape 2
  \end{enumerate}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Algorithme --- version itérative en pseudocode}

  Nous allons remplacer «une valeur de départ quelconque» par une
  valeur déterminée.

  \begin{minipage}{0.6\linewidth}
    \begin{Schunk}
\begin{Verbatim}
sqrt(réel x)
  y <- 1
  Tant que y^2 !~= x
    y <- (y + x/y)/2
  Fin Tant que
  Retourner y
Fin sqrt
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Algorithme --- version récursive en pseudocode}

  \begin{minipage}{0.6\linewidth}
    Pfff! Facile!

    \begin{Schunk}
\begin{Verbatim}
sqrt(réel x)
  y <- 1
  Si y^2 ~= x
    Retourner y
  Retourner sqrt((y + x/y)/2)
Fin sqrt
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
  \pause

  \alert{\bfseries NOT!}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Algorithme --- version récursive en pseudocode (prise 2)}

  Il faut utiliser deux procédures.

  \begin{minipage}{0.6\linewidth}
    \begin{Schunk}
\begin{Verbatim}
sqrt(réel x)
  sqrt-iter(1, x)
Fin sqrt

sqrt-iter(y, x)
  Si y^2 ~= x
    Retourner y
  Retourner sqrt-iter((y + x/y)/2, x)
Fin sqrt-iter
\end{Verbatim}
    \end{Schunk}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Nombre d'opérations}

  Mathématiquement, nous avons
  \begin{equation*}
    1 + x + x^2 + x^3 + x^4 = 1 + x(1 + x(1 + x(1 + x(1)))).
  \end{equation*}
  \pause
  Numériquement, par contre:
  \begin{itemize}
  \item calculs répétés inutilement avec l'équation de gauche
  \item \alert{plus efficace} d'utiliser l'équation de droite
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Temps d'exécution}

  Le tri par insertion (que nous étudierons au chapitre 7) est un algorithme $O(N^2)$.
\begin{Schunk}
\begin{lstlisting}
> x <- sample(1:10000, 10000, replace = TRUE)
> system.time(insertionsort(x))
utilisateur     système      écoulé
      5.019       0.250       5.283

> x <- sample(1:20000, 20000, replace = TRUE)
> system.time(insertionsort(x))
utilisateur     système      écoulé
     19.676       1.056      20.779
\end{lstlisting}
\end{Schunk}
  \pause
\begin{Schunk}
\begin{lstlisting}
> system.time(sort(x))
utilisateur     système      écoulé
      0.001       0.000       0.002
\end{lstlisting}
\end{Schunk}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "programmation-avec-r-analyse-donnees"
%%% coding: utf-8
%%% End:
