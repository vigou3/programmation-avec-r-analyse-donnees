%%% Copyright (C) 2019-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «IFT-4902 Programmation avec R pour l'analyse de données»
%%% https://gitlab.com/vigou3/programmation-avec-r-analyse-donnees
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Analyse et contrôle de texte}

\begin{frame}
  \frametitle{Expressions régulières}

  Opérateurs essentiels.

  \begin{center}
    \includegraphics[height=0.85\textheight,keepaspectratio=true]{images/regex-cheatsheet}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Outils}

  \begin{itemize}
  \item Les outils standards Unix travaillent sur \alert{une ligne à
      la fois} d'un fichier texte
  \item Difficulté principale: plusieurs «variétés» des outils
    \begin{itemize}
    \item expressions régulières «de base» ou «étendues»
    \item versions de \code{sed}
    \item macOS (utilitaires BSD plutôt que GNU)
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{\code{grep}}
  \setlength{\unitlength}{10mm}

  Sélectionne les lignes qui correspondent à une expression régulière.

  \begin{Schunk}
\begin{Verbatim}
$ grep -E '^###? .+' bases.R
\end{Verbatim}
  \end{Schunk}
  \pause
  \begin{picture}(14,3)
    \visible<2>{%
      \put(2.0,3.3){\line(0,-1){3.3}}
      \put(2.1,0.0){expressions régulières «étendues» ou «modernes»}
      \put(3.5,3.3){\line(0,-1){2.3}}
      \put(3.6,1.0){motif}
      \put(5.3,3.3){\line(0,-1){1.3}}
      \put(5.5,2.0){fichier(s) à traiter}
    }
    \visible<3>{%
      \thicklines
      \put(1.15,3.3){\line(0,-1){1.3}}
      \put(1.25,2.0){\alert{%
          \begin{minipage}[t]{10cm}
            extrait du fichier \code{bases.R} les lignes débutant
            par deux ou trois symboles «\#» suivis d'une espace et
            d'au moins un autre caractère
          \end{minipage}}}
    }
  \end{picture}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\code{sed}}
  \setlength{\unitlength}{10mm}

  Recherche et remplace du texte sur une ligne (surtout).

  \visible<2>{%
    \begin{picture}(14,0.5)
      \put(2,-0.35){\line(0,1){0.6}}
      \put(4.25,-0.35){\line(0,1){0.25}}
      \put(5.4,-0.35){\line(0,1){0.25}}
      \put(2,-0.1){\line(1,0){3.4}}
      \put(2.1,0){délimiteur des arguments (symbole quelconque)}
    \end{picture}}
  \begin{Schunk}
\begin{Verbatim}
$ sed 's/foo\([^b]\)/abc\1/g' bases.R
\end{Verbatim}
  \end{Schunk}
  \pause
  \begin{picture}(14,3)
    \visible<2>{%
      \put(1.8,3.3){\line(0,-1){3.3}}
      \put(1.9,0.0){commande «rechercher et remplacer»}
      \put(3.0,3.3){\line(0,-1){2.8}}
      \put(3.1,0.5){motif à rechercher}
      \put(4.75,3.3){\line(0,-1){2.3}}
      \put(4.85,1.0){chaine de remplacement}
      \put(5.8,3.3){\line(0,-1){1.8}}
      \put(5.9,1.5){toutes les occurrences («global»)}
      \put(7.0,3.3){\line(0,-1){1.3}}
      \put(7.1,2.0){fichier à traiter}
    }
    \visible<3>{%
      \thicklines
      \put(1.15,3.3){\line(0,-1){1.3}}
      \put(1.25,2.0){\alert{%
          \begin{minipage}[t]{10cm}
            remplace toutes les occurrences de la chaine «foo», mais
            pas de «foobar», dans \code{bases.R} par «abc» et le
            caractère qui suit «foo»
          \end{minipage}}}
    }
  \end{picture}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\code{awk}}
  \setlength{\unitlength}{10mm}

  Traite du texte séparé en champs ou sur plusieurs lignes.

  \visible<2>{%
    \begin{picture}(14,0.5)
      \put(4.6,-0.35){\line(0,1){0.6}}
      \put(4.7,0){numéro du champ}
    \end{picture}}
  \begin{Schunk}
\begin{Verbatim}
$ awk '/^20/ { print $2 }' 100metres.data
\end{Verbatim}
  \end{Schunk}
  \pause
  \begin{picture}(14,3)
    \visible<2>{%
      \put(2.25,3.3){\line(0,-1){3.3}}
      \put(2.35,0.0){motif (expression régulière ou logique)}
      \put(4.0,3.3){\line(0,-1){2.3}}
      \put(4.1,1){action}
      \put(6.8,3.3){\line(0,-1){1.3}}
      \put(6.9,2.0){fichier à traiter}
    }
    \visible<3>{%
      \thicklines
      \put(1.15,3.3){\line(0,-1){1.3}}
      \put(1.25,2.0){\alert{%
          \begin{minipage}[t]{10cm}
            extrait la deuxième colonne des lignes débutant par «20»
            dans le fichier \code{100metres.data}, soit les temps des
            records survenus dans les années 2000
          \end{minipage}}}
    }
  \end{picture}
\end{frame}

\begin{frame}
  \frametitle{À retenir}

  \begin{itemize}
  \item Puissance du mode texte brut
    \begin{itemize}
    \item facilite le traitement automatisé (scriptage)
    \item facilite l'échange et le contrôle de versions
    \end{itemize}
  \item Puissance des programmes chainés avec les opérateur Unix
    \begin{itemize}
    \item transfert de données d'un programme à un autre avec «\textbar»
    \item redirection de la sortie d'un programme vers un fichier avec «\code{>}»
    \item redirection d'un fichier vers l'entrée d'un programme avec
      «\code{<}»
    \end{itemize}

  \end{itemize}
\end{frame}


%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "programmation-avec-r-analyse-donnees"
%%% coding: utf-8
%%% End:
