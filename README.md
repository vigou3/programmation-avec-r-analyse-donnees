<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Programmation avec R pour l'analyse de données

Ce projet contient les diapositives des ateliers en classe du cours [IFT-4902 Programmation avec R pour l'analyse de données](https://www.ulaval.ca/etudes/cours/ift-4902-programmation-avec-r-pour-lanalyse-de-donnees) offert par l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

Le cours offre une introduction à l'algorithmique et à la programmation avec le langage R pour les analystes de données. Les objectifs généraux sont:

1. développer une culture de l'informatique;
2. acquérir la capacité à résoudre des problèmes concrets à l'aide de l'algorithmique et de la programmation;
3. se familiariser avec les bonnes pratiques reconnues en contexte de travail collaboratif.

L'ouvrage de référence du cours est [*Programmer avec R*](https://vigou3.gitlab.io/programmer-avec-r).

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«Programmation avec R pour l'analyse de données» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Modèle de développement

Le processus de rédaction et de maintenance du projet suit le modèle [*Gitflow Workflow*](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow ). Seule particularité: la branche *master* se trouve dans le dépôt [`programmation-avec-r-analyse-donnees`](https://gitlab.com/vigou3/programmation-avec-r-analyse-donnees) dans GitLab, alors que la branche de développement se trouve dans le dépôt [`programmation-avec-r-analyse-donnees-devel`](https://projets.fsg.ulaval.ca/git/scm/vg/programmation-avec-r-analyse-donnees-devel) dans le serveur BitBucket de la Faculté des sciences et de génie de l'Université Laval.

Prière de passer par le dépôt `programmation-avec-r-analyse-donnees-devel` pour proposer des modifications; consulter le fichier `CONTRIBUTING.md` pour la marche à suivre.

## Obtenir les diapositives

Pour obtenir les diapositives en format PDF, consulter la [page des versions](https://gitlab.com/vigou3/programmation-avec-r-analyse-donnees/-/releases) (*releases*).
