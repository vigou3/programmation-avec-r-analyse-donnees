%%% Copyright (C) 2019-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «IFT-4902 Programmation avec R pour l'analyse de données»
%%% https://gitlab.com/vigou3/programmation-avec-r-analyse-donnees
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Boucles itératives}

\begin{frame}
  \frametitle{Avertissement}

  Avant d'utiliser une boucle dans R, rappelez-vous l'Axiome de la
  programmation en R:

  \begin{center}
    \begin{minipage}{0.15\linewidth}
      \raggedright%
      arithmétique vectorielle
    \end{minipage}
    \quad$+$\quad
    \begin{minipage}{0.15\linewidth}
      \raggedright%
      fonctions d'application
    \end{minipage}
    \quad$=$\quad
    \begin{minipage}{0.3\linewidth}
      \raggedright%
      programmation (presque) sans boucles \alert{explicites}
    \end{minipage}
  \end{center}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Des structures néanmoins utiles}

  Trois grandes manières de concevoir les boucles itératives:
  \begin{enumerate}
  \item Répéter exactement $n$ fois
    \begin{Schunk}
\begin{Verbatim}[commandchars=\\\{\}]
for (\meta{variable} in \meta{suite})
    \meta{expression}
\end{Verbatim}
    \end{Schunk}
  \item Répéter $0$ ou plusieurs fois
    \begin{Schunk}
\begin{Verbatim}[commandchars=\\\{\}]
while (\meta{condition})
    \meta{expression}
\end{Verbatim}
    \end{Schunk}
  \item Répéter au moins $1$ fois
    \begin{Schunk}
\begin{Verbatim}[commandchars=\\\{\}]
repeat
    \meta{expression}
\end{Verbatim}
    \end{Schunk}
  \end{enumerate}
\end{frame}

\begin{frame}[plain]
  \tipbox{\textbf{Soyez explicite!} Utilisez le type de boucle qui
    indique immédiatement au lecteur ce que réalise le code.}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Quelques remarques}

  \begin{itemize}
  \item Dans une boucle \code{for}, \meta{suite} n'a pas à prendre ni
    des valeurs consécutives, ni des valeurs numériques
    \begin{Schunk}
\begin{Verbatim}
for (i in 1:10)
    ...
\end{Verbatim}
    \end{Schunk}
    \begin{Schunk}
\begin{Verbatim}
for (i in c(1, 27, 5, 42))
    ...
\end{Verbatim}
    \end{Schunk}
    \begin{Schunk}
\begin{Verbatim}
for (str in c("Pierre", "Jean", "Jacques"))
    ...
\end{Verbatim}
    \end{Schunk}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Quelques remarques (suite)}

  \begin{itemize}
  \item Utiliser \\
    \medskip
    \begin{minipage}{0.45\linewidth}
      \begin{Schunk}
\begin{Verbatim}
for (i in seq_len(n))
    ...
\end{Verbatim}
      \end{Schunk}
      plutôt que
      \begin{Schunk}
\begin{Verbatim}
for (i in 1:n)
    ...
\end{Verbatim}
      \end{Schunk}
    \end{minipage}
    \hfill
    \begin{minipage}{0.45\linewidth}
      \begin{Schunk}
\begin{Verbatim}
for (i in seq_along(x))
    ...
\end{Verbatim}
      \end{Schunk}
      plutôt que
      \begin{Schunk}
\begin{Verbatim}
for (i in 1:length(x))
    ...
\end{Verbatim}
      \end{Schunk}
    \end{minipage}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Quelques remarques (suite)}

  \begin{itemize}
  \item Une boucle \code{repeat} \alert{doit} contenir un test et
    utiliser \code{break} pour sortir de la boucle
    \begin{Schunk}
\begin{Verbatim}[commandchars=\\\[\]]
repeat
{
    ...
    if (\meta[condition])
        break
}
\end{Verbatim}
    \end{Schunk}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Exemple}

  Le \alert{tri par sélection} requiert des boucles à dénombrement.

  \begin{algorithm}
    \smallskip%
    Trier les données $x_1, x_2, \dots, x_n$ en ordre croissant.
    \begin{enumerate}
    \item Répéter les étapes 2 et 3 pour $i = 1, 2, \dots, n - 1$.
    \item Trouver l'indice $j$ du plus petit élément parmi
      $x_i, x_{i + 1}, \dots, x_n$.
    \item Inverser les données $x_j \leftrightarrow x_i$.
    \item Retourner les données réordonnées.
    \end{enumerate}
  \end{algorithm}
\end{frame}

\begin{frame}
  \frametitle{Exemple (suite)}

  Illustration: trier les nombres $86\; 72\; 12\; 55\; 79$.

  \begin{center}
    \begin{tabular}{*{6}{c}}
      \multicolumn{1}{|c}{86} & 72 & \textbf{12} & 55 & 79 & $(i = 1)$ \\
      12 & \multicolumn{1}{|c}{72} & 86 & \textbf{55} & 79 & $(i = 2)$ \\
      12 & 55 & \multicolumn{1}{|c}{86} & \textbf{72} & 79 & $(i = 3)$ \\
      12 & 55 & 72 & \multicolumn{1}{|c}{86} & \textbf{79} & $(i = 4)$ \\
      12 & 55 & 72 & 79 & \multicolumn{1}{|c}{86} \\
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Exemple (suite)}

  Illustration: trier les nombres $86\; 72\; 12\; 55\; 79$.

  \begin{center}
    \begin{tabular}{*{6}{c}}
      \multicolumn{1}{|c}{86} & 72 & \textbf{12} & 55 & 79 & $(i = 1)$ \\
      12 & \multicolumn{1}{|c}{72} & 86 & \textbf{55} & 79 & $(i = 2)$ \\
         & $\uparrow$              &    & $\uparrow$ \\
         & $x_2$                   &    & $x_4$ \\
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Exemple (suite)}

  Mise en œuvre
  \begin{Schunk}
\begin{lstlisting}[basicstyle=\small\ttfamily\NoAutoSpacing]
`{\only<2>{\color{alert}}selectionsort <- function(x)}'
`{\only<2>{\color{alert}}\{}'
    `{\only<3>{\color{alert}}xlen <- length(x)}'
    `{\only<4>{\color{alert}}for (i in seq\_len(xlen - 1))}'
    `{\only<4>{\color{alert}}\{}'
        `{\only<5>{\color{alert}}i.min <- i}'
        `{\only<5>{\color{alert}}for (j in i:xlen)}'
        `{\only<5>{\color{alert}}\{}'
            `{\only<5>{\color{alert}}if (x[j] < x[i.min])}'
                `{\only<5>{\color{alert}}i.min <- j}'
        `{\only<5>{\color{alert}}\}}'
        `{\only<6>{\color{alert}}x[c(i, i.min)] <- x[c(i.min, i)]}'
    `{\only<4>{\color{alert}}\}}'
    `{\only<2>{\color{alert}}x}'
`{\only<2>{\color{alert}}\}}'
\end{lstlisting}
  \end{Schunk}
\end{frame}


%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "programmation-avec-r-analyse-donnees"
%%% coding: utf-8
%%% End:
